package com.gspann.feignclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@EnableFeignClients
@Controller
@SpringBootApplication
public class FeignclientApplication {

	@Autowired
	private HelloClient helloClient;

	public static void main(String[] args) {
		SpringApplication.run(FeignclientApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello() {
		return helloClient.hello();
	}
}
