package com.gspann.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="serviceone", url="http://localhost:8761")
public interface HelloClient {
	@RequestMapping("/hello")
	String hello();
}