package com.gspann.serviceone.controller;

import org.springframework.web.bind.annotation.GetMapping;

public interface HelloController {
	@GetMapping("/hello")
	String hello();
	
}
